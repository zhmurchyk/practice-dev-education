package com.github.numbers;

public class Fibonacci {

    public static void main(String[] args) {

        int n = 11;
        System.out.println("The raw of first " + n + " elements of Fibonacci is:");
        fibonacci(n);
    }

    public static void fibonacci(int n){
        long x = 1;
        long y = 1;
        System.out.print(y);
        for (int i = 1; i < n; i++) {
            System.out.print(" " + y);
            y = x + y;
            x = y - x;

        }
    }
}
