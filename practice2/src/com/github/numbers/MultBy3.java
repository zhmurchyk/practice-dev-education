package com.github.numbers;

public class MultBy3 {

    public static void main(String[] args) {

        System.out.println("The three times table: ");
        for(int i = 1; i < 11; i++){
            System.out.println(i + " * 3 = " + i*3);
        }
    }
}
