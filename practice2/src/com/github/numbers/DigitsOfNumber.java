package com.github.numbers;

import com.github.discriminant.ScannerWrapper;

public class DigitsOfNumber {

    public static void main(String[] args) {
        System.out.println("Enter a number: ");
        int n = ScannerWrapper.getNumber();
        System.out.println("The sum of number's digits is " + sumOfDigits(n));
        int res = multOfDigits( n, 1 );
        System.out.println("The result of multiplication of digits is " + res);
        System.out.println("The biggest digit is " + biggestNumber(n));
    }

    public static int sumOfDigits(int n){
        int sum = 0;
        for ( ; n > 0; n /= 10)
            sum += n % 10;
        return sum;
    }

    private static int multOfDigits ( int x, int mult ) {
        if ( x == 0 )
            return mult;
        return multOfDigits( x / 10, x % 10 * mult );
    }

    public static int biggestNumber(int n){
        int max = 0;
        while (n > 0) {
            byte curDigit = (byte)(n % 10);
            if (curDigit > max)
                max = curDigit;
            n /= 10;
        }
        return max;
    }
}
