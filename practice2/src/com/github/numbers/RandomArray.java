package com.github.numbers;

public class RandomArray {

    public static void main(String[] args) {
        System.out.println("Size of randomArray: ");
        int size = (int)(Math.random() * 10);
        System.out.println(size);
        int[] randomArray = new int[size];
        System.out.println("Array of numbers: ");
        for (int i = 0; i < size; i++) {
            randomArray[i] = (int)(Math.random() * 10);
            System.out.print(" " + randomArray[i]);

        }
        System.out.println();
        System.out.println("Result of sum of two numbers from array: ");
        int result = (int)(Math.random() * 10);
        System.out.println(result);
        int[] pairsArray = pairsFinder(randomArray, size, result);

        System.out.println("The pair of numbers is: ");
        for (int i = 1; i < pairsArray[0]; i = i+2) {
            System.out.println(pairsArray[i] + " + " + pairsArray[i + 1] + " = " + result);
        }
    }

    public static int[] pairsFinder(int[] randomArray, int size, int result){
        int[] pairs = new int[(size*(size-1))/2+2];
        int k = 1;
        int count = 0;
        for(int i = 0; i < size; i++){
            for(int j = i+1; j < size; j++){
                if(randomArray[i] + randomArray[j] == result) {
                    pairs[k]= randomArray[i] ;
                    pairs[k+1] = randomArray[j];
                    k = k+2;
                    count= count+2;
                }
            }
        }
        pairs[0] = count;
        return pairs;
    }
}
