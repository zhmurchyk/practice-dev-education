package com.github.loops;

public class Loop2 {

    public static void main(String[] args) {
        int a = -20;
        int b = 20;
        System.out.println("Print with whileLoop method: ");
        whileMethod(a,b);
        System.out.println();
        System.out.println("Print with forLoop method: ");
        forMethod(a, b);
    }

    public static void whileMethod(int a, int b) {
        int i = a;
        while(i < b+1) {
            System.out.print(" " + i);
            i++;
        }
    }

    public static void forMethod(int a, int b) {
        int i;
        for(i = a; i < b+1; i++) {
            System.out.print(" " + i);
        }
    }
}
