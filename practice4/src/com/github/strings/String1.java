package com.github.strings;

import com.github.discriminant.ScannerWrapper;

public class String1 {

    public static void main(String[] args) {

        String str;
        char[] ch = {',', '.', ':', '?', '!'};
        System.out.println("Enter str: ");
        str = ScannerWrapper.getString();
        StringBuilder sb = new StringBuilder();
        sb.append(str);
        for (int i = 0; i < sb.length()-1; i++) {
            for (char c : ch) {
                if (sb.charAt(i) == c) {
                    if (!sb.substring(i + 1, i + 2).equals(" ")) {
                        sb.insert(i + 1, ' ');
                    }
                }
            }
        }
        System.out.println(sb.toString());
    }

}
