package com.github.fourNumbers;

public class Math {
    public static int findMin(int[] a){
        int min = a[0];
        for(int i = 1; i < a.length; i++){
            if(a[i] < min){
                min = a[i];
            }
        }
        return min;
    }

    static int amoutMax(int[] a) {
        int max = findMax(a);
        int count = 0;
        for(int i = 0; i < a.length; i++){
            if(a[i] == max){
                count++;
            }
        }
        return count;
    }

    private static int findMax(int[] a) {
        int max = a[0];
        for(int i = 1; i < a.length; i++){
            if(a[i] > max){
                max = a[i];
            }
        }
        return max;
    }
}
