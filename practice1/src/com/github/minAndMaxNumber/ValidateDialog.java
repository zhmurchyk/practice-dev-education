package com.github.minAndMaxNumber;

import com.github.discriminant.ScannerWrapper;

public class ValidateDialog {

    static int[] arr = new int[5];

    public static void validateDialog(){
        int a;
        int count = 0;
        do{
            System.out.println("Enter a number: ");
            a = ScannerWrapper.getNumber();
            arr[count] = a;
            count++;
        } while (count < arr.length);
    }
}
