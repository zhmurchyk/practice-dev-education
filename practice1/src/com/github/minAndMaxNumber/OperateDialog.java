package com.github.minAndMaxNumber;

public class OperateDialog {

    public static void operateDialog() {
        ValidateDialog.validateDialog();
        int[] arr = ValidateDialog.arr;
        System.out.println("Minimal number is: " + Math.findMin(arr));
        System.out.println("Maximal number is: " + Math.findMax(arr));
    }
}
