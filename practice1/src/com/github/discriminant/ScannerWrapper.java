package com.github.discriminant;

import java.util.Scanner;

public class ScannerWrapper {
    private static final Scanner scanner = new Scanner(System.in);

    public static int getNumber() {
        return scanner.nextInt();
    }

    public static String getString() {
        return scanner.nextLine();
    }
}

