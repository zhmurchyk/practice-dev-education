package com.github.sameNames;

public class SameNames {

    public static boolean sameNames(String a, String b){
        a = a.toLowerCase();
        b = b.toLowerCase();
        return a.equals(b);
    }
}
