package com.github.arrays;

public class Arrays7 {

    public static void main(String[] args) {

        int[] arr = new int[12];
        int max = -15;
        for (int i = 0; i < 12; i++) {
            arr[i] = ((int)(Math.random() * 31)-15);
            System.out.print(" " + arr[i]);
            if (arr[i] > max) max = arr[i];
        }
        System.out.println("\nMaximal element is: " + max);
    }
}
