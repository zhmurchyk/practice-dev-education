package com.github.arrays;

public class Arrays6 {

    public static void main(String[] args) {

        int n = 20;
        System.out.println("The array of first " + n + " elements of Fibonacci is:");
        fibonacci(n);
    }

    public static void fibonacci(int n){
        int x = 1;
        int y = 1;
        int[] arr = new int[20];
        arr[0] = 1;
        System.out.print(arr[0]);
        for (int i = 1; i < n; i++) {
            arr[i] = y;
            System.out.print(" " + arr[i]);
            y = x + y;
            x = y - x;
        }
    }
}
